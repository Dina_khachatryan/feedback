<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Repository\FeedbackRepositoryInterface;
use Illuminate\Http\Request;



class FeedbackController extends Controller
{
    private $feedbackRepository;

    public function __construct(FeedbackRepositoryInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }

    public function index(){
        return view('main');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'message' => 'required'
        ]);

        Feedback::create([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'message' => $request->get('message'),
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Feedback has been sent successfully!'
        ], 200);
    }

}
