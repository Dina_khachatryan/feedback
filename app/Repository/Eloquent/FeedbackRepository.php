<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 12/1/2020
 * Time: 3:33 PM
 */

namespace App\Repository\Eloquent;

use App\Models\Feedback;
use App\Repository\FeedbackRepositoryInterface;
use Illuminate\Support\Collection;

class FeedbackRepository extends BaseRepository implements FeedbackRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(Feedback $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }
}