<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Feedback</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <!-- Styles -->
        <style>
            body {
                font-family: Times New Roman, serif;
                background-color: #d8d8d8;
            }
            .container{
                width: 700px;
                height: 550px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                border: 2px solid #b9c1cb;
                padding: 30px 0px;
                box-shadow: 4px 4px 5px 5px rgba(34, 60, 80, 0.5);
                background-color: #f4f4f4;
                margin-top: 50px;
                margin-bottom: 50px;
            }
            h1{
                color: #4a5568;
                font-weight: 700;
                margin: 30px 0px;
            }
            label{
                color: #4a5568;
            }
            .form-group{
                width: 70%;
            }
            .container p{
                color: red;
                font-size: 10px;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <feedback></feedback>
        </div>

    </body>
</html>
<script type="text/javascript" src="/js/app.js"></script>
